﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using MusicPlayerClient.ServicePlayer;

namespace MusicPlayerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var impl = new Connector();
            using (ServiceHost host = new ServiceHost(impl, new Uri(@"http://127.0.0.1:9090/Notebook")))
            {
                var ep = host.AddServiceEndpoint(typeof(IServicePlayer), new WebHttpBinding(), "");
                ep.EndpointBehaviors.Add(new WebHttpBehavior());
                ServiceMetadataBehavior behavior = host.Description.Behaviors.Find<ServiceMetadataBehavior>();
                if (behavior == null)
                {
                    behavior = new ServiceMetadataBehavior { HttpGetEnabled = true };
                    host.Description.Behaviors.Add(behavior);
                }
                host.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");
                host.Opening += new EventHandler(Host_Opening);
                host.Opened += new EventHandler(Host_Opened);
                host.Open();

                Console.ReadLine();
                Console.ReadKey();
            }

        }
        static void Host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("Service is ready!\nPress <ENTER> key to terminate service....\n");
        }

        static void Host_Opening(object sender, EventArgs e)
        {
            Console.WriteLine("Opening service.....");
        }


        //var factory = new ChannelFactory<IServicePlayer>(new WebHttpBinding(),
        //    new EndpointAddress("http://localhost:12345/ServicePlayer.svc"));
        //factory.Endpoint.Behaviors.Add(new WebHttpBehavior());
        //Console.WriteLine(factory.Endpoint);
        //var proxy = factory.CreateChannel();
        //Console.WriteLine(proxy.GetLoadableList());

        }
    }
}
